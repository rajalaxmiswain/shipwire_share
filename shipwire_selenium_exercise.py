#! usr/bin/python

"""
Run the tests with: python shipwire_selenium_exercise.py


This test script:
1. Goes to https://beta.shipwire.com/
2. Clicks on the "LOG IN" link and logs in as a test user.
username:shiptest@mailinator.com / password: test1234 and clicks the "Sign in" button.
3. Clicks on the "upport" link at the top of the navigation.
4. That will open a new tab "How can we help you today?". It clicks on the "Contact Us" link.
5. On the Contact page, fills out all required information and clicks the "Get in touch" button.
 Verifies that	a "Thank you" overlay is displayed after submitting the form.

"""

from selenium									 import webdriver
from selenium.webdriver.support.ui				 import WebDriverWait
from selenium.webdriver.common.by				 import By
from selenium.webdriver.common.keys				 import Keys
from selenium.webdriver.common.action_chains	 import ActionChains
from selenium.webdriver.support.select			 import Select
from selenium.webdriver.support					 import expected_conditions as EC

import unittest,time,sys



class SelectDropDownOption(unittest.TestCase):

	def setUp(self):
		self.driver = webdriver.Firefox()
		self.driver.implicitly_wait(10)
		self.base_url = "https://beta.shipwire.com/"
		self.driver.get(self.base_url)
		self.driver.maximize_window()
		#assertion to confirm page title, else AssertionError is raised
		self.assertEqual("Order fulfillment for ecommerce | Shipwire - !DEVEL!", self.driver.title)

   
	 
	def test_shipwire_login_contact_us_flow(self):
		driver = self.driver
		logInLinkLocator	  = "(//a)[9]"
		logInLinkElement	  = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(logInLinkLocator))
		logInLinkElement.click()
		
		
		#switch focus to iFrame to then fill out username, password and click LogIn
		iFrameID  = "login-iframe"
		driver.switch_to.frame(iFrameID)
		#emailFieldLocator		= driver.find_element_by_xpath("//input[@name='username']")
		emailFieldLocator		= "#login-user-name"
		emailFieldElement	   = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_css_selector(emailFieldLocator))
		emailFieldElement.click()
		emailFieldElement.send_keys("shiptest@mailinator.com")

				
		passwordFieldLocator	  = "#password"
		passwordFieldElement	  = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_css_selector(passwordFieldLocator))					 
		passwordFieldElement.click()
		passwordFieldElement.send_keys("test1234")
		
		
		signInButtonLocator			 = "(//button) [@class = 'cta-button'][1]"
		#signInButtonLocator		 = "html/body/div[1]/div/header/nav[2]/ul/li[5]/div/div[2]/form/button"
		signInButtonElement			 = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(signInButtonLocator))
		signInButtonElement.click()
		
		
		
		supportTabLocator	   = "(//a)[@class = 'shipwireLink']"
		supportTabElement	   = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(supportTabLocator)) 
		#assert page title  = "https://merchant.beta.shipwire.com/merchants/account/dashboard"
		self.assertEqual('Shipwire', driver.title)


		#Get the main Window handle
		mainWindowHandle = driver.window_handles
		#print "main Window handle: %s" %mainWindowHandle
		#clicking supportTab will open a new window 'http://www.shipwire.com/support/'
		supportTabElement.click()


		allWindowsHandlesList	 = driver.window_handles
		#shift focus to the new window http://www.shipwire.com/support/	 to be able to interact with that page
		#print "all window handles: %s" %allWindowsHandlesList
		for handle in allWindowsHandlesList:
			if handle != mainWindowHandle[0]:
				driver.switch_to.window(handle)
				break


		#assert page title = "http://www.shipwire.com/support/" 
		self.assertEqual('Shipwire Support - Logistics & Fulfillment Services', driver.title)
		
		contactUsTabLocator	   = "(//a)[5]"
		contactUsTabElement	   = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(contactUsTabLocator))
		contactUsTabElement.click()


		#assert page title = "https://app.shipwire.com/contact/?itd=contact&iti=shared_topbar"
		self.assertEqual('Contact Shipwire To Find Out How Our Order Fulfillment Services Can Help You Grow', driver.title)
		
		
		
		#click on the "What can we help you with?" DropDownList on https://app.shipwire.com/contact/?itd=contact&iti=shared_topbar 
		# and then select option "Partners"
		helpDropDownList			 = driver.find_element_by_xpath("//div[@id='message-type-select']").click()
		helpdropDownOptionLocator	 = "//div[@id='message-type-select']/ul/li[4]"
		helpdropDownOptionElement	 = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(helpdropDownOptionLocator))
		helpdropDownOptionElement.click()


		#Fill out fields on the Contact Us page (name, email, phone num, company, What can we do for you? fields)
		nameFieldLocator		 =	"//input[@name='name']"
		nameFieldElement		 = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(nameFieldLocator))
		nameFieldElement.click()
		nameFieldElement.send_keys("Test Man")



		emailFieldLocator		   = "//input[@name='email']"
		emailFieldElement		   = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(emailFieldLocator))
		emailFieldElement.click()
		emailFieldElement.send_keys("shiptest@abc.com")
		
		


		phoneNumberFieldLocator		= "//input[@name='phone']"
		phoneNumberFieldElement		= WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(phoneNumberFieldLocator))
		phoneNumberFieldElement.click()
		phoneNumberFieldElement.send_keys("18002223333")



		companyFieldLocator			= "//input[@name='company']"
		companyFieldElement			= WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(companyFieldLocator))
		companyFieldElement.click()
		companyFieldElement.send_keys("N/A")



		#Enter text in 'What can we do for you?' field
		questionTextAreaLocator		  = "//textarea[@name='question']"
		questionTextAreaElement		  = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(questionTextAreaLocator))
		questionTextAreaElement.click()
		questionTextAreaElement.send_keys("I appreciate your service. \n Thank you for your help.")



		#Now click getInTouchButton
		getInTouchButtonLocator		  = "//button[@class ='cta-button contact-button']"
		getInTouchButtonElement		  = WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(getInTouchButtonLocator))
		getInTouchButtonElement.click()



		#alert = driver.switch_to_alert()
		#verify the "Thank You for contacting us,  Someone will be in touch with you shortly" overlay image displayed on the page after clicking getInTouchButton
		thankYouImageFrameLocator		= "//div[@id='contact-thank-you']/h4/img"
		thankYouImageFrameElement		= WebDriverWait(driver, 10).\
							 until(lambda driver: driver.find_element_by_xpath(thankYouImageFrameLocator))

		if EC.visibility_of_element_located(thankYouImageFrameLocator):
			print "'Thank You' overlay image displayed on the page, Test PASSED"
		else:
			message = "'Thank You' overlay image NOT FOUND, Test FAILED" 
			raise Exception(message)

				
   
	
	def tearDown(self):
		self.driver.quit()
		
		

if __name__ == "__main__":
   unittest.main()
